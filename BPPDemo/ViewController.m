//
//  ViewController.m
//  BPPDemo
//
//  Created by Phil Peters on 22/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import "ViewController.h"
#import <BPPPEDSDK/BPPPEDSDK.h>
#import <PEDSDK/PEDSDK.h>

@interface ViewController ()
@property (nonatomic, retain) NSString * host;
@property (nonatomic, retain) NSString * apiKey;

@property (nonatomic, retain) BPPConnection * conn;
@property (nonatomic, retain) IBOutlet UILabel * statusLabel;
@property (nonatomic, retain) IBOutlet UIView * connectedView;
@property (nonatomic, retain) IBOutlet UIButton * payGBP1Button;
@property (nonatomic, retain) IBOutlet UIButton * payGBP1_5Button;
@property (nonatomic, retain) IBOutlet UIButton * payGBP10Button;
@property (nonatomic, retain) IBOutlet UIButton *cancelButton;
@property (nonatomic, retain) IBOutlet UIButton *upgradeButton;
@property (nonatomic, retain) IBOutlet UILabel * actionStatus;

@property (nonatomic, retain) IBOutlet UITextField * refundReference;
@property (nonatomic, retain) IBOutlet UITextField * refundAmount;
@property (nonatomic, retain) IBOutlet UIButton *refundButton;

@property (nonatomic, retain) IBOutlet PEDPaymentRequest * currentPayment;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.host = @"https://bpp.withbolt.com/c/bpp-staging";
    self.host = @"https://bpp.withbolt.com/c/bpp";
    self.apiKey = @"2e2d3700dd36366a0eedb4282db1da58";
    
    [self clearConnection];
}

- (void) clearConnection {
    self.conn = nil;
    self.currentPayment = nil;
    self.statusLabel.text = @"";
    self.actionStatus.text = @"EMPTY";

    [self.view setNeedsLayout];
}

- (IBAction) connectTest:(id)sender {
    NSLog(@"Connect Test");
    [self clearConnection];
    
    ViewController * _self = self;
    self.conn = [BPPConnection connectionToHost:self.host usingApiKey:self.apiKey accountCode:@"demo" paymentMethod:@"test" statusUpdateBlock:^(BPPConnection * conn) {
        NSLog(@"Status Update Callback");
        [_self.view setNeedsLayout];
    } configDelegate:self];
}

- (IBAction) connectPaypal:(id)sender {
    NSLog(@"Connect Paypal");
    [self clearConnection];

    ViewController * _self = self;
    // Ids:
    // 25 =
    // 37 = PROD
//    self.conn = [BPPConnection connectionToHost:self.host usingApiKey:self.apiKey accountCode:@"styalways" paymentMethod:@"9" statusUpdateBlock:^(BPPConnection * conn) {
    self.conn = [BPPConnection connectionToHost:self.host usingApiKey:self.apiKey accountCode:@"willyworms" paymentMethod:@"19" statusUpdateBlock:^(BPPConnection * conn) {
//    self.conn = [BPPConnection connectionToHost:self.host usingApiKey:self.apiKey accountCode:@"demo" paymentMethod:@"paypal"/*25,27*/ statusUpdateBlock:^(BPPConnection * conn) {
//    self.conn = [BPPConnection connectionToHost:self.host usingApiKey:self.apiKey accountCode:@"iglookids" paymentMethod:@"17"/*15,17*/ statusUpdateBlock:^(BPPConnection * conn) {
//    self.conn = [BPPConnection connectionToHost:self.host usingApiKey:self.apiKey accountCode:@"dancingdaisyltd" paymentMethod:@"12" statusUpdateBlock:^(BPPConnection * conn) {
NSLog(@"Status Update Callback");
        [_self.view setNeedsLayout];
    } configDelegate:self];
//    self.conn = [BPPConnection connectionToHost:self.host usingApiKey:self.apiKey accountCode:@"posdemo" paymentMethod:@"3" statusUpdateBlock:^(BPPConnection * conn) {
//        NSLog(@"Status Update Callback");
//        [_self.view setNeedsLayout];
//    }];
}

- (IBAction) connectiZettle:(id)sender {
    NSLog(@"Connect iZettle");
    [self clearConnection];
    
    ViewController * _self = self;
    self.conn = [BPPConnection connectionToHost:self.host usingApiKey:self.apiKey accountCode:@"demo" paymentMethod:@"izettle" statusUpdateBlock:^(BPPConnection * conn) {
        NSLog(@"Status Update Callback");
        [_self.view setNeedsLayout];
    } configDelegate:self];
}

- (IBAction) connectAdyen:(id)sender {
    NSLog(@"Connect Adyen");
    [self clearConnection];
    
    ViewController * _self = self;
    self.conn = [BPPConnection connectionToHost:self.host usingApiKey:self.apiKey accountCode:@"pwbtestuk" paymentMethod:@"17" statusUpdateBlock:^(BPPConnection * conn) {
//        self.conn = [BPPConnection connectionToHost:self.host usingApiKey:self.apiKey accountCode:@"demo" paymentMethod:@"adyen" statusUpdateBlock:^(BPPConnection * conn) {
        NSLog(@"Status Update Callback");
        [_self.view setNeedsLayout];
    } configDelegate:self];
}

- (IBAction) connectUSAEpay:(id)sender {
    NSLog(@"Connect USAEpay");
    [self clearConnection];
    
    ViewController * _self = self;
    self.conn = [BPPConnection connectionToHost:self.host usingApiKey:self.apiKey accountCode:@"pwbtestuk" paymentMethod:@"7" statusUpdateBlock:^(BPPConnection * conn) {
        NSLog(@"Status Update Callback");
        [_self.view setNeedsLayout];
    } configDelegate:self];
}

- (IBAction) connectMoneris:(id)sender {
    NSLog(@"Connect Moneris");
    [self clearConnection];
    
    ViewController * _self = self;
    self.conn = [BPPConnection connectionToHost:self.host usingApiKey:self.apiKey accountCode:@"outerheaven" paymentMethod:@"2" statusUpdateBlock:^(BPPConnection * conn) {
        NSLog(@"Status Update Callback");
        [_self.view setNeedsLayout];
    } configDelegate:self];
}


- (IBAction) payGBP1:(id)sender {
    NSString * transactionId = [NSUUID UUID].UUIDString;

    NSLog(@"payGBP1: %@", transactionId);
    self.refundReference.text = transactionId;
    
    self.actionStatus.text = @"";
    ViewController * _self = self;
    self.currentPayment = [self.conn startPaymentInCurrency:@"CAD" forAmount:[NSDecimalNumber decimalNumberWithString:@"1"] withTransactionReference:transactionId salesOrderCustomerReference:@"SO#00003" statusUpdateBlock:^(PEDPaymentRequest *request) {
        _self.actionStatus.text = request.error ? [NSString stringWithFormat:@"%@: %@", request.status, request.error.localizedDescription] : request.status;

        if (![request.status isEqualToString:kPEDPaymentStatusInProgress]) {
            NSLog(@"Payment Complete");
            _self.currentPayment = nil;
        }
        
        [_self.view setNeedsLayout];
    }];
}

- (IBAction) payGBP1_5:(id)sender {
    NSString * transactionId = [NSUUID UUID].UUIDString;

    NSLog(@"payGBP1_5: %@", transactionId);
    
    self.refundReference.text = transactionId;

    self.actionStatus.text = @"";
    ViewController * _self = self;
    self.currentPayment = [self.conn startPaymentInCurrency:@"CAD" forAmount:[NSDecimalNumber decimalNumberWithString:@"1.50"] withTransactionReference:transactionId salesOrderCustomerReference:@"SO#00003" statusUpdateBlock:^(PEDPaymentRequest *request) {
        NSLog(@"Payment Complete");
        _self.actionStatus.text = request.error ? [NSString stringWithFormat:@"%@: %@", request.status, request.error.localizedDescription] : request.status;

        if (![request.status isEqualToString:kPEDPaymentStatusInProgress]) {
            NSLog(@"Payment Complete");
            _self.currentPayment = nil;
        }
        
        [_self.view setNeedsLayout];
    }];
}

- (IBAction) payGBP10:(id)sender {
    NSString * transactionId = [NSUUID UUID].UUIDString;

    NSLog(@"payGBP10: %@", transactionId);
    
    self.refundReference.text = transactionId;
    self.actionStatus.text = @"";
    ViewController * _self = self;
    self.currentPayment = [self.conn startPaymentInCurrency:@"GBP" forAmount:[NSDecimalNumber decimalNumberWithString:@"10"] withTransactionReference:transactionId salesOrderCustomerReference:@"SO#00003" statusUpdateBlock:^(PEDPaymentRequest *request) {
        NSLog(@"Payment Complete");
        _self.actionStatus.text = request.error ? [NSString stringWithFormat:@"%@: %@", request.status, request.error.localizedDescription] : request.status;

        if (![request.status isEqualToString:kPEDPaymentStatusInProgress]) {
            NSLog(@"Payment Complete");
            _self.currentPayment = nil;
        }
        
        [_self.view setNeedsLayout];
    }];
}

- (IBAction) refundPayment:(id)sender {
    NSLog(@"refundPayment %@", self.refundAmount.text);
    
    self.actionStatus.text = @"";
    ViewController * _self = self;
    [self.conn refundTransactionReference:self.refundReference.text amount:(self.refundAmount.text.length ? [NSDecimalNumber decimalNumberWithString:self.refundAmount.text] : nil)  withReason:@"Test Refund" statusUpdateBlock:^(PEDRefundRequest *request) {
        NSLog(@"Refund Complete");
        _self.actionStatus.text = request.error ? [NSString stringWithFormat:@"%@: %@", request.status, request.error.localizedDescription] : request.status;
        
        if (![request.status isEqualToString:kPEDPaymentStatusInProgress]) {
            NSLog(@"Refund Complete");
        }
        
        [_self.view setNeedsLayout];
    }];
}

- (IBAction) cancelPayment:(id)sender {
    NSLog(@"cancelPayment");
    self.actionStatus.text = @"";
    [self.conn cancelPayment:self.currentPayment];
    self.currentPayment = nil;
}

- (IBAction) upgradeFirmware:(id)sender {
    NSLog(@"upgradeFirmware");
    self.actionStatus.text = @"";
    ViewController * _self = self;
    
    [self.conn updateFirmwareOnComplete:^(BOOL success, NSString *message) {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:success ? @"Firmware Update Success" : @"Firmware Update Failed" message:message preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction: [UIAlertAction
                       actionWithTitle:@"OK"
                       style:UIAlertActionStyleDefault
                       handler:^(UIAlertAction * action)
                       {
                           [alert dismissViewControllerAnimated:YES completion:nil];
                           
                       }]];
        [_self presentViewController:alert animated:true completion:^{
        }];
    }];
}


- (void) viewWillLayoutSubviews {
    NSString * status = [NSString stringWithFormat:@"%@%@", self.conn.ped.status, self.conn.ped.error ? [NSString stringWithFormat:@": %@", self.conn.ped.error.localizedDescription] : @""];
    
    BOOL showButtons = self.conn && ([self.conn.ped.status isEqualToString:kPEDStatusConnectedReady] ||
                                     [self.conn.ped.status isEqualToString:kPEDStatusConnectedOffline] ||
                                     [self.conn.ped.status isEqualToString:kPEDStatusConnectedUpgrade] ||
                                     [self.conn.ped.status isEqualToString:kPEDStatusConnectedNoPed]);
    self.statusLabel.text = self.conn.ped ? [NSString stringWithFormat:@"%@\n%@", self.conn.ped.pedDescription, status] : @"";
    
    self.connectedView.hidden = !showButtons && ![self.conn.ped.status isEqualToString:kPEDStatusConnectedReady];
    self.cancelButton.hidden = !showButtons && !([self.conn.ped.status isEqualToString:kPEDStatusConnectedReady] && self.currentPayment);
}

- (NSString *)valueFor:(NSString *)key {
    NSLog(@"%@ = %@", key, [[NSBundle mainBundle] objectForInfoDictionaryKey:key]);
    return (NSString *)[[NSBundle mainBundle] objectForInfoDictionaryKey:key];
}

@end
