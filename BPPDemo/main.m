//
//  main.m
//  BPPDemo
//
//  Created by Phil Peters on 22/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
